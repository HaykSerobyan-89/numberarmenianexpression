
<h1 align="center"> :zap: HackTech Task :zap:</h1>

![](https://img.shields.io/badge/Python-3.9.7-brightgreen) 

# __Task description__ 
## We are building a product which has a part of automatically generating invoices. As part of it, we need a functionality which will convert any integer in the range of (0, 999999999) to Armenian words expressions.

# __Acceptance criteria__

#### _Input_: 
##### 456741
#### Output: 
##### չորս հարյուր հիսունվեց հազար յոթ հարյուր քառասունմեկ

#
#### Input: 
##### 0
#### Output: 
##### զրո

#
#### Input: 
##### “8”
#### Output: 
##### Error:  Type is not int

#
#### Input: 
##### 8.0
#### Output: 
##### Error:  Type is not int

#
#### Input: 
##### 111
#### Output: 
##### մեկ հարյուր տասնմեկ


